using System.Web.Mvc;

namespace Spa.Controllers
{
    public class JasmineController : Controller
    {
        public ViewResult Index()
        {
            return Run("App");
        }

        public ViewResult Samples()
        {
            return Run("SpecRunner");
        }

        public ViewResult Run(string id)
        {
            return View(id);
        }
    }
}
