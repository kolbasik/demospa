﻿using System.Web.Optimization;

namespace Spa
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Ignore("_references.js", OptimizationMode.Always);
            bundles.IgnoreList.Ignore("*.intellisense.js", OptimizationMode.Always);
            bundles.IgnoreList.Ignore("*-vsdoc.js", OptimizationMode.Always);
            bundles.IgnoreList.Ignore("*.debug.js", OptimizationMode.Always);
            bundles.IgnoreList.Ignore("*.min.js", OptimizationMode.Always);

            var cssRewriteUrlTransform = new CssRewriteUrlTransformFixVirtualPath(new CssRewriteUrlTransformImagesBase64Wise());

            // NOTE: vedors scripts
            bundles.Add(new StyleBundle("~/bundles/vendors/css")
                .Include("~/Content/bootstrap.css", cssRewriteUrlTransform)
                .Include("~/Content/bootstrap-theme.css", cssRewriteUrlTransform)
            );

            bundles.Add(new ScriptBundle("~/bundles/vendors/js").Include(
                "~/Scripts/lodash.js",
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/angular.js",
                "~/Scripts/angular-resource.js",
                "~/Scripts/angular-ui-router.js"
            ));

            // NOTE: app scripts
            bundles.Add(new AngularTemplateBundle("~/bundles/app/templates", templatesAlias: "app.templates")
                .IncludeDirectory("~/Content/common", "*.html", true)
                .IncludeDirectory("~/Content/pages", "*.html", true)
            );

            bundles.Add(new StyleBundle(@"~/bundles/app/css")
                .IncludeDirectory("~/Content/common", "*.css", true)
                .IncludeDirectory("~/Content/pages", "*.css", true)
            );

            bundles.Add(new IgnorableScriptBundle(@"~/bundles/app/js", ignoreFileExtension: @".spec.js")
                .IncludeDirectory("~/Content/common", "*.js", true)
                .IncludeDirectory("~/Content/pages", "*.js", true)
            );

            bundles.Add(new Bundle("~/bundles/app/spec")
                .IncludeDirectory("~/Content/common", "*.spec.js", true)
                .IncludeDirectory("~/Content/pages", "*.spec.js", true)
            );
        }
    }
}
