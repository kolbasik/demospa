﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Optimization;

namespace Spa
{
    public class CssRewriteUrlTransformImagesBase64Wise : IItemTransform
    {
        public string Process(string includedVirtualPath, string input)
        {
            if (includedVirtualPath == null)
                throw new ArgumentNullException("includedVirtualPath");

            return ConvertUrlsToAbsolute(VirtualPathUtility.GetDirectory(includedVirtualPath.Substring(1)), input);
        }

        internal static string ConvertUrlsToAbsolute(string baseUrl, string content)
        {
            if (string.IsNullOrWhiteSpace(content))
                return content;
            return new Regex("url\\(['\"]?(?<url>[^)]+?)['\"]?\\)").Replace(content, match => "url(" + RebaseUrlToAbsolute(baseUrl, match.Groups["url"].Value) + ")");
        }

        internal static string RebaseUrlToAbsolute(string baseUrl, string url)
        {
            if (string.IsNullOrWhiteSpace(url) || string.IsNullOrWhiteSpace(baseUrl) || url.StartsWith("/", StringComparison.OrdinalIgnoreCase)
                || url.StartsWith("data:", StringComparison.OrdinalIgnoreCase) || url.StartsWith("http", StringComparison.OrdinalIgnoreCase))
                return url;
            if (!baseUrl.EndsWith("/", StringComparison.OrdinalIgnoreCase))
                baseUrl = baseUrl + "/";
            return VirtualPathUtility.ToAbsolute(baseUrl + url);
        }
    }
}