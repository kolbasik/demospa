﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Optimization;
using Newtonsoft.Json;

namespace Spa
{
    public sealed class AngularTemplateBundleBuilder : IBundleBuilder
    {
        private readonly string _baseRoot;
        private readonly string _templatesAlias;

        public AngularTemplateBundleBuilder(string baseRoot, string templatesAlias = null)
        {
            _baseRoot = baseRoot ?? string.Empty;
            _templatesAlias = templatesAlias ?? "templates";
        }

        public string BuildBundleContent(Bundle bundle, BundleContext context, IEnumerable<BundleFile> files)
        {
            if (files == null)
            {
                return string.Empty;
            }

            if (bundle == null) throw new ArgumentNullException("bundle");
            if (context == null) throw new ArgumentNullException("context");

            var sb = new StringBuilder();
            sb.AppendLine(@"angular.module(""" + _templatesAlias + @""", []).run([""$templateCache"", function($templateCache) {");

            foreach (var bundleFile in files)
            {
                var virtualPath = bundleFile.VirtualFile.VirtualPath;
                if (!string.IsNullOrEmpty(_baseRoot))
                {
                    virtualPath = Regex.Replace(virtualPath, "^" + _baseRoot, string.Empty);
                }
                var template = JsonConvert.SerializeObject(bundleFile.ApplyTransforms());
                sb.AppendFormat(@"$templateCache.put(""{0}"",{1});", virtualPath, template).Append(bundle.ConcatenationToken);
            }

            sb.AppendLine(@"}]);");

            return sb.ToString();
        }
    }
}