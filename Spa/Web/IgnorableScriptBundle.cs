using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Optimization;

namespace Spa
{
    public sealed class IgnorableScriptBundle : ScriptBundle
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IgnorableScriptBundle"/> class.
        /// </summary>
        /// <param name="virtualPath">The virtual path.</param>
        /// <param name="ignoreFileExtension">The ignore file extension.</param>
        /// <exception cref="System.ArgumentNullException"></exception>
        public IgnorableScriptBundle(string virtualPath, string ignoreFileExtension)
            : base(virtualPath)
        {
            if(string.IsNullOrWhiteSpace(System.IO.Path.GetExtension(ignoreFileExtension)))
                throw new ArgumentNullException(ignoreFileExtension);

            IgnoreFileExtension = ignoreFileExtension;
        }

        /// <summary>
        /// Gets the ignore file extension.
        /// </summary>
        /// <value>
        /// The ignore file extension.
        /// </value>
        public string IgnoreFileExtension { get; private set; }

        /// <summary>
        /// Generates an enumeration of <see cref="T:System.Web.Hosting.VirtualFile" /> objects that represent the contents of the bundle.
        /// </summary>
        /// <param name="context">The <see cref="T:System.Web.Optimization.BundleContext" /> object that contains state for both the framework configuration and the HTTP request.</param>
        /// <returns>
        /// An enumeration of <see cref="T:System.Web.Hosting.VirtualFile" /> objects that represent the contents of the bundle.
        /// </returns>
        public override IEnumerable<BundleFile> EnumerateFiles(BundleContext context)
        {
            return base.EnumerateFiles(context).Where(bundleFile => !bundleFile.VirtualFile.VirtualPath.EndsWith(IgnoreFileExtension));
        }
    }
}