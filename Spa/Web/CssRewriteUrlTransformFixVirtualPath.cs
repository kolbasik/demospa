﻿using System;
using System.Web.Optimization;

namespace Spa
{
    public class CssRewriteUrlTransformFixVirtualPath : IItemTransform
    {
        private readonly IItemTransform _transform;

        public CssRewriteUrlTransformFixVirtualPath(IItemTransform itemTransform)
        {
            if (itemTransform == null) throw new ArgumentNullException("itemTransform");

            _transform = itemTransform;
        }

        public string Process(string includedVirtualPath, string input)
        {
            return _transform.Process("~" + includedVirtualPath, input);
        }
    }
}