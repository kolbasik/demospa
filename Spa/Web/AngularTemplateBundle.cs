﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Optimization;

namespace Spa
{
    public sealed class AngularTemplateBundle : Bundle
    {
        private readonly IItemTransform _htmlMinifyItemTransform;

        /// <summary>
        ///     Initializes a new instance of the <see cref="T:System.Web.Optimization.ScriptBundle" /> class that takes virtual
        ///     path and cdnPath for the bundle.
        /// </summary>
        /// <param name="virtualPath">The virtual path for the bundle.</param>
        /// <param name="baseRoot">The base root.</param>
        /// <param name="templatesAlias">The templates alias.</param>
        public AngularTemplateBundle(string virtualPath, string baseRoot = null, string templatesAlias = null)
            : base(virtualPath)
        {
            Builder = new AngularTemplateBundleBuilder(baseRoot ?? VirtualPathUtility.ToAbsolute("~/"), templatesAlias);
            ConcatenationToken = Environment.NewLine;
            _htmlMinifyItemTransform = new HtmlMinify();
        }

        /// <summary>
        /// Applies the transforms.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="bundleContent">Content of the bundle.</param>
        /// <param name="bundleFiles">The bundle files.</param>
        /// <returns></returns>
        public override BundleResponse ApplyTransforms(BundleContext context, string bundleContent, IEnumerable<BundleFile> bundleFiles)
        {
            var response = base.ApplyTransforms(context, bundleContent, bundleFiles);
            if (string.IsNullOrWhiteSpace(response.ContentType))
            {
                response.ContentType = @"text/javascript";
            }
            return response;
        }

        public override IEnumerable<BundleFile> EnumerateFiles(BundleContext context)
        {
            foreach (var bundleFile in base.EnumerateFiles(context))
            {
                if (!bundleFile.Transforms.Contains(_htmlMinifyItemTransform))
                    bundleFile.Transforms.Add(_htmlMinifyItemTransform);
                yield return bundleFile;
            }
        }
    }

    public sealed class HtmlMinify : IItemTransform
    {
        public string Process(string includedVirtualPath, string input)
        {
            return Minify(input);
        }

        /// <summary>
        /// Minifies the given HTML string.
        /// </summary>
        /// <param name="html">
        /// The html to minify.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string Minify(string html)
        {
            // Replace line comments
            html = Regex.Replace(html, @"// (.*?)\r?\n", "", RegexOptions.Singleline);
            // Replace spaces between quotes
            html = Regex.Replace(html, @"\s+", " ");
            // Replace line breaks
            html = Regex.Replace(html, @"\s*\n\s*", "\n");
            // Replace spaces between brackets
            html = Regex.Replace(html, @"\s*\>\s*\<\s*", "> <"); // NOTE: one space should be presented due to the inline-block issue.
            // Replace comments
            html = Regex.Replace(html, @"<!--(?!\[)(.*?)-->", "");
            return html.Trim();
        }
    }
}