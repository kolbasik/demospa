﻿/// <reference path="../../_references.d.ts" />
"use strict";

module ymap {
    export class YMapRouter {
        constructor($stateProvider) {
            $stateProvider
                .state("ymap", {
                    url: "/ymap",
                    template: "<div id='ymap'></div>",
                    controller: YMapController
                });
        }
    }
    YMapRouter.$inject = ["$stateProvider"];

    class YMapController {
        private map: YMaps.Map;
        private placemark: YMaps.Placemark;

        constructor() {
            var self = this;
            function init() {
                self.map = new ymaps.Map("ymap", {
                    center: [55.76, 37.64],
                    zoom: 7
                });

                self.placemark = new ymaps.Placemark([55.76, 37.64], {
                    content: "Moscow!",
                    balloonContent: "Capital of Russia"
                });

                self.map.geoObjects.add(self.placemark);
            }
            window["ymaps"] && ymaps.ready(init);
        }
    }

    YMapController.$inject = ["common"];

    angular.module("ymap", ["ui.router", "app.common"]).config(YMapRouter);
}