﻿/// <reference path="../_references.d.ts" />
/// <chutzpah_reference path="../_references.js" />
"use strict";

module northwind {

    export class ProductsController {
        product: IProduct;
        products: IProduct[];

        constructor(
            private page: app.IPage,
            private northwindService: INorthwindService) {
            this.activate();
        }

        private activate() {
            this.page.isBusy = true;
            this.bind([]);
            this.load()
                .then(data => this.bind(data))
                .finally(() => this.page.isBusy = false);
        }

        private load() {
            return this.northwindService.getAllProducts();
        }

        private bind(products: IProduct[]) {
            this.products = products || [];
            this.product = this.products[0];
        }
    }
    ProductsController.$inject = ["page", "northwind.NorthwindService"];

    angular.module("northwind.Products", ["app.common", "northwind.common"])
        .controller("northwind.ProductsController", ProductsController);
}