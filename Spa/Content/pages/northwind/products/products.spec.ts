﻿/// <reference path="../_references.d.ts" />
/// <reference path="products.ts" />
/// <chutzpah_reference path="../_references.js" />
/// <chutzpah_reference path="products.js" />
"use strict";

describe("Northwind:Products module",() => {
    beforeEach(module("northwind.Products"));

    describe("ProductsController",() => {
        var $rootScope,
            $controller;

        beforeEach(inject((_$rootScope_, _$controller_) => {
            $rootScope = _$rootScope_;
            $controller = _$controller_;
        }));

        it("should exist.",() => {
            var scope = $rootScope.$new();
            var ctrl = $controller("northwind.ProductsController", { $scope: scope });

            expect(ctrl).toBeTruthy();
        });

        describe("#activate",() => {

            var northwindService,
                page: app.IPage,
                ctrl: northwind.ProductsController;

            beforeEach(inject((_$q_, _page_) => {
                northwindService = {
                    getAllProducts: (() => {
                        var deffered = _$q_.defer();
                        var execute: any = () => deffered.promise;
                        execute.deffered = deffered;
                        return execute;
                    })()
                }
                page = _page_;
                ctrl = $controller("northwind.ProductsController", {
                    $scope: $rootScope.$new(),
                    "northwind.NorthwindService": northwindService
                });
            }));

            it("should load and bind data.", done => {
                var expected = [{ ID: 0 }, { ID: 1 }, { ID: 2 }];
                var deffered = northwindService.getAllProducts.deffered;

                expect(page.isBusy).toBeTruthy();

                deffered.resolve(expected);
                deffered.promise.finally(() => window.setTimeout(assert));
                $rootScope.$digest();

                function assert() {
                    expect(ctrl.products).toEqual(expected);
                    expect(ctrl.product).toEqual(expected[0]);
                    expect(page.isBusy).toBeFalsy();
                    done();
                };
            });
        });
        
    });
});