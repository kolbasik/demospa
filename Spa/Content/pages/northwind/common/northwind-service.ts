﻿/// <reference path="../../../_references.d.ts" />
"use strict";

module northwind {

    class NorthwindService implements INorthwindService {
        constructor(private common: app.ICommon) {}

        private getResourcePath(resource: string): string {
            return "http://services.odata.org/V3/Northwind/Northwind.svc/" + resource + "?$format=json";
        }

        getAllEmployees(): ng.IPromise<IEmployee[]> {
            return this.common.$http
                .get<INorthwindServerResponse<IEmployee[]>>(this.getResourcePath("Employees"))
                .then(response => {
                    return response.data.value || [];
                });
        }

        getAllProducts(): ng.IPromise<IProduct[]> {
            return this.common.$http
                .get<INorthwindServerResponse<IProduct[]>>(this.getResourcePath("Products"))
                .then(response => {
                    return response.data.value || [];
                });
        }
    }
    NorthwindService.$inject = ["common"];

    interface INorthwindServerResponse<T> {
        value: T
    }

    angular.module("northwind.common", ["app.common"])
        .service("northwind.NorthwindService", NorthwindService);
}