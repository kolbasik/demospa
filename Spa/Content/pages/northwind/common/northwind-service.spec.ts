﻿/// <reference path="../_references.d.ts" />
/// <chutzpah_reference path="../_references.js" />
"use strict";

describe("Northwind commmon module",() => {
    beforeEach(module("northwind.common"));

    describe("NorthwindService", () => {
        var northwindService: northwind.INorthwindService;

        beforeEach(inject($injector => {
            northwindService = $injector.get("northwind.NorthwindService");
        }));

        it("should exist.", () => {
            expect(northwindService).toBeTruthy();
        });

        describe("#getAllEmployees",() => {
            var $httpBackend;

            beforeEach(inject(_$httpBackend_ => {
                $httpBackend = _$httpBackend_;
            }));

            it("should send an HTTP POST request.", () => {
                $httpBackend.expectGET("http://services.odata.org/V3/Northwind/Northwind.svc/Employees?$format=json")
                    .respond(200, { value: [] });

                northwindService.getAllEmployees();

                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });

            it("should return a value from HTTP response.", done => {
                var expected = [{ EmployeeID: 0 }];

                $httpBackend.expectGET("http://services.odata.org/V3/Northwind/Northwind.svc/Employees?$format=json")
                    .respond(200, { value: expected });

                northwindService.getAllEmployees().then(actual => {
                    expect(actual).toEqual(expected);
                    done();
                });

                $httpBackend.flush();
            });
        });

        describe("#getAllProducts",() => {
            var $httpBackend;

            beforeEach(inject(_$httpBackend_ => {
                $httpBackend = _$httpBackend_;
            }));

            it("should send an HTTP POST request.", () => {
                $httpBackend.expectGET("http://services.odata.org/V3/Northwind/Northwind.svc/Products?$format=json")
                    .respond(200, { value: [] });

                northwindService.getAllProducts();

                $httpBackend.flush();
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });

            it("should return a value from HTTP response.", done => {
                var expected = [{ ID: 0 }];

                $httpBackend.expectGET("http://services.odata.org/V3/Northwind/Northwind.svc/Products?$format=json")
                    .respond(200, { value: expected });

                northwindService.getAllProducts().then(actual => {
                    expect(actual).toEqual(expected);
                    done();
                });

                $httpBackend.flush();
            });
        });
        
    });
});