﻿/// <reference path="../../../_references.d.ts" />

declare module northwind {

    export interface INorthwindService {
        getAllEmployees(): ng.IPromise<IEmployee[]>;
        getAllProducts(): ng.IPromise<IProduct[]>;
    }

    export interface IEmployee {
        EmployeeID: number;
        LastName: string;
        FirstName: string;
        Title: string;
        TitleOfCourtesy: string;
        BirthDate: string;
        HireDate: string;
        Address: string;
        City: string;
        Region: string;
        PostalCode: string;
        Country: string;
        HomePhone: string;
        Extension: string;
        Photo: string;
        Notes: string;
        ReportsTo: number;
        PhotoPath: string;
    }

    export interface IProduct {
        ID: number;
        Name: string;
        Description: string;
        ReleaseDate: string;
        DiscontinuedDate: string;
        Rating: number;
        Price: number;
    }
}