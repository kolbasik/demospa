﻿/// <reference path="../../_references.d.ts" />
"use strict";

module northwind {

    export class NorthwindRouter {
        constructor($stateProvider) {
            $stateProvider
                .state("northwind", {
                    'abstract': true,
                    url: "/northwind",
                    templateUrl: "Content/pages/northwind/northwind.html"
                })
                .state("northwind.employees", {
                    url: "/employees",
                    templateUrl: "Content/pages/northwind/employees/employees.html",
                    controller: EmployeesController,
                    resolve: EmployeesController.resolve
                })
                .state("northwind.products", {
                    url: "/products",
                    templateUrl: "Content/pages/northwind/products/products.html",
                    controllerAs: "vm",
                    controller: "northwind.ProductsController"
                });
        }
    }
    NorthwindRouter.$inject = ["$stateProvider"];

    angular.module("northwind", ["ui.router", "app.common", "northwind.Employees", "northwind.Products"])
        .config(NorthwindRouter);
}