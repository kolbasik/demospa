﻿/// <reference path="../../../_references.d.ts" />
"use strict";

module northwind {

    export interface IEmployeesPage extends app.IRootScope {
        employees: IEmployee[];
        employee: IEmployee;
    }

    export class EmployeesController {
        static $inject = ["$scope", "employees"];

        static resolve = {
            employees: [
                "northwind.NorthwindService",
                (northwindService: INorthwindService): ng.IPromise<IEmployee[]> =>
                    northwindService.getAllEmployees()
            ]
        };

        constructor(
            $scope: IEmployeesPage,
            employees: IEmployee[]) {
            $scope.employees = employees;
            $scope.employee = employees[0];
        }
    }

    angular.module("northwind.Employees", ["northwind.common"])
        .controller("northwind.EmployeesController", EmployeesController);
}