﻿/// <reference path="../_references.d.ts"/>
"use strict";

module app {

    class RrdRouter {
        constructor($urlRouterProvider) {
            $urlRouterProvider.otherwise("/ymap");
        }
    }
    RrdRouter.$inject = ["$urlRouterProvider"];

    angular.module("app.templates", []); // NOTE: trick for the debug mode.
    angular.module("rrd", ["ui.router", "app.common", "app.templates", "ymap", "northwind"])
        .config(RrdRouter);
}