﻿/// <reference path="../_references.d.ts"  />
/// <chutzpah_reference path="../_references.js" />
/// <chutzpah_reference path="common.js" />
"use strict";

/* Links
 * http://blogs.endjin.com/2014/09/unit-testing-angularjs-with-visual-studio-resharper-and-teamcity/
 * http://www.smashingmagazine.com/2014/10/07/introduction-to-unit-testing-in-angularjs/
 * https://docs.angularjs.org/api/auto/service/$injector
 */
describe("Application commmon module", () => {
    beforeEach(module("app.common"));

    describe("Page", () => {
        var page: app.IPage;

        beforeEach(inject(function (_page_) {
            page = _page_;
        }));

        it("should exist.",() => {
            expect(page).toBeTruthy();
        });

        it("should have isBusy boolean property.",() => {
            expect(page.isBusy).toBeDefined();
            expect(page.isBusy).toBeFalsy();
        });
    });

    describe("Common",() => {
        var common: app.ICommon;

        beforeEach(inject(function (_common_) {
            common = _common_;
        }));

        it("should exist.",() => {
            expect(common).toBeTruthy();
        });

        it("should have a link to the jQuery",() => {
            expect(common.$).toBeDefined();
        });

        it("should have a link to the lodash.",() => {
            expect(common._).toBeDefined();
        });

        it("should have a link to the angluar:$rootScope.",() => {
            expect(common.$rootScope).toBeDefined();
        });

        it("should have a link to the angluar:$http.",() => {
            expect(common.$http).toBeDefined();
        });

        it("should have a link to the angluar:$log.",() => {
            expect(common.$log).toBeDefined();
        });

        it("should have a link to the angluar:$window.",() => {
            expect(common.$window).toBeDefined();
        });

        it("should have a link to the angluar:$timeout.",() => {
            expect(common.$timeout).toBeDefined();
        });

        it("should have a link to the angluar:$filter.",() => {
            expect(common.$filter).toBeDefined();
        });

        it("should have a link to the angluar-ui:$state.",() => {
            expect(common.$state).toBeDefined();
        });

        it("should have a link to the app.common:page.",() => {
            expect(common.page).toBeDefined();
        });
    });
});