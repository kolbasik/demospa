﻿/// <reference path="../../Scripts/_references.d.ts" /> 

declare module app {

    export interface IPage {
        isBusy: boolean
    }

    export interface IRootScope extends ng.IRootScopeService {
        _: _.LoDashStatic;
        $state: any;
        page: IPage;
    }

    export interface ICommon {
        // vendors
        _: _.LoDashStatic;
        $: JQueryStatic;

        // angular
        $rootScope: ng.IRootScopeService;
        $http: ng.IHttpService;
        $log: ng.ILogService;
        $window: ng.IWindowService;
        $timeout: ng.ITimeoutService;
        $filter: ng.IFilterService;

        // ui.router
        $state: ng.ui.IStateService;

        // foundation
        page: IPage;
    }

} 