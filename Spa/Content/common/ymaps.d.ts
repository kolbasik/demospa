﻿/* Yandex Maps
 * https://api.yandex.com/maps/doc/jsapi/2.x/quick-start/tasks/quick-start.xml
 */
declare module YMaps {

    export interface ILocation extends Array<number> {
    }

    export interface IMapOptions {
        center: ILocation;
        zoom: number;
    }

    export interface Map {
        (target: string, options: IMapOptions): void;
        geoObjects: { add (placemark: Placemark); }
    }

    export interface IPlacemarkOptions {
        content: string;
        balloonContent: string;
    }

    export interface Placemark {
        (location: ILocation, options: IPlacemarkOptions): void;
    }

    export interface YMapsStatic {
        Map: Map;
        Placemark: Placemark;
        ready(callback: Function);
    }
}
declare var ymaps: YMaps.YMapsStatic; 