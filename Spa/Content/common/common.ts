﻿/// <reference path="../../Scripts/_references.d.ts" /> 
"use strict";

module app {

    class Page implements IPage {
        isBusy: boolean;

        constructor() {
            this.isBusy = false;
        }
    }

    class Common implements ICommon {
        _: _.LoDashStatic;
        $: JQueryStatic;

        static $inject = ["$rootScope",
            "$http",
            "$log",
            "$window",
            "$timeout",
            "$filter",
            "$state",
            "page"
        ];

        constructor(
            // angular
            public $rootScope: IRootScope,
            public $http: ng.IHttpService,
            public $log: ng.ILogService,
            public $window: ng.IWindowService,
            public $timeout: ng.ITimeoutService,
            public $filter: ng.IFilterService,

            // ui.router
            public $state,

            // foundation
            public page: IPage
        ) {
            var global: any = $window;

            // lodash
            this._ = global._;

            // jquery
            this.$ = global.$ || global.jQuery;

            // NOTE: sets the visibility of services in bindings
            $rootScope._ = this._;
            $rootScope.page = this.page;
            $rootScope.$state = $state;

            // NOTE: just for tests
            global['_TEST_COMMON_'] = this;
        }
    }

    angular.module("app.common", ["ui.router"])
        .service("page", Page)
        .service("common", Common);
} 